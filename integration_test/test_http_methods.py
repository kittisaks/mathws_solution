from mathWs_test import MathWsTest
from mathWs_test_utils import *

class TestHttpMethod(MathWsTest):

    def getTestSuiteName(self):
        return "Test Http Method"

    def __tc__test_get_NoRequestBody_expectError(self):
        #Prepare
        expectedRes = MathWsResponse()
        expectedRes.setHttp(405, "This HTTP method is not allowed")

        #Execute
        actualRes = testGet()

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_post_invalidService_expectError(self):

        #Prepare
        service = "invalidService"
        operation = "add"

        expectedRes = MathWsResponse()
        expectedRes.setHttp(400, "Invalid service name")
        expectedRes.setService(service, operation)

        #Execute
        actualRes = testPostWithParams("test_post_invalidService_expectError", service, operation, [], [])

        #verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_post_invalidOperation_expectError(self):

        #Prepare
        service = "simple"
        operation = "adda"

        expectedRes = MathWsResponse()
        expectedRes.setHttp(400, "Invalid Simple service operation")
        expectedRes.setService(service, operation)

        #Execute
        actualRes = testPostWithParams("test_post_invalidOperation_expectError", service, operation, [], [])

        #verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_post_ValidService_expectNoError(self):
        #Prepare
        service = "simple"
        operation = "add"
        a = [1.1, 2.2, 3.3, 4.4]
        b = [5.5, 6.6, 7.7, 8.8]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)

        #Execute
        actualRes = testPostWithParams("test_post_ValidService_expectNoError", service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))
