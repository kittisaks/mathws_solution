import sys
import test_http_methods
import test_simple_service

def runAllTestSuites(testSuites):
    totalCnt = 0
    passedCnt = 0
    failedCnt = 0
    for suite in testSuites:
        resultList = suite.runTestSuite()
        for result in resultList:
            if (result[2]):
                totalCnt += 1
                passedCnt += 1
            else:
                totalCnt += 1
                failedCnt += 1

    print "\n*** Overall result ***"
    print "\tTotal cases:  " + str(totalCnt)
    print "\tPassed cases: " + str(passedCnt)
    print "\tFailed cases: " + str(failedCnt)
    if (failedCnt == 0):
        print "\t\033[1m\033[92mTEST PASSED\033[0m"
    else:
        print "\t\033[1m\033[91mTEST FAILED\033[0m"
        sys.exit("Exit with error")

if __name__ == "__main__":

    testSuites = [
        test_http_methods.TestHttpMethod(),
        test_simple_service.TestSimpleService()
    ]

    runAllTestSuites(testSuites)

