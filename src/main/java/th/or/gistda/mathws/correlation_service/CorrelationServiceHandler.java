package th.or.gistda.mathws.correlation_service;

import java.util.ArrayList;
import java.util.List;

import th.or.gistda.mathws.base.ELogType;
import th.or.gistda.mathws.base.HttpConst;
import th.or.gistda.mathws.base.IMathWsLogger;
import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.service_selector.IServiceHandler;

public class CorrelationServiceHandler implements IServiceHandler {

	private IMathWsLogger logger = null;
	
	public CorrelationServiceHandler(IMathWsLogger logger) {
		this.logger = logger;
	}
	
	@Override
	public void handle(IMathWsRequest req, IMathWsResponse res) {
		
		if (!req.getOperation().equals("normal")) {
			res.setService(req.getName(), req.getOperation());
			res.setHttp(HttpConst.STATUS_BAD_REQUEST, "Invalid correlation service operation");
			logger.log(req, ELogType.info, "[CorrelationServiceHandler]: Invalid operation: " + req.getOperation());
			return;
		}
		
		List<Double> inputA = req.getInputA();
		List<Double> inputB = req.getInputB();
		List<Double> outputC = new ArrayList<Double>();
		
		correlation_normal(inputA, inputB, outputC);
		
		res.setService(req.getName(), req.getOperation());
		res.setHttp(HttpConst.STATUS_OK, "Operation completed successfully");
		res.setInput(inputA, inputB);
		res.setOutput(outputC);
		logger.log(req, ELogType.info, "[CorrelationServiceHandler]: Operation completed successfully");
	}
	
	protected void correlation_normal(List<Double> inputA, List<Double> inputB, List<Double> outputC) {
		Double acc = 0.0;
		for (int idx=0;idx<inputA.size();idx++) {
			acc += inputA.get(idx) * inputB.get(idx);
		}
		outputC.add(acc);
	}

}
