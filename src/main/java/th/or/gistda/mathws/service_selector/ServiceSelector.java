package th.or.gistda.mathws.service_selector;

import th.or.gistda.mathws.base.ELogType;
import th.or.gistda.mathws.base.HttpConst;
import th.or.gistda.mathws.base.IMathWsLogger;
import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.convolution_service.ConvolutionServiceHandler;
import th.or.gistda.mathws.correlation_service.CorrelationServiceHandler;
import th.or.gistda.mathws.simple_service.SimpleServiceHandler;

public class ServiceSelector implements IServiceSelector {

	private IMathWsLogger logger = null;
	
	public ServiceSelector(IMathWsLogger logger) {
		this.logger = logger;
	}
	
	@Override
	public void process(IMathWsRequest req, IMathWsResponse res) {
		
		EService servName = EService.parse(req.getName());
		
		if (servName == null) {
			res.setService(req.getName(), req.getOperation());
			res.setHttp(HttpConst.STATUS_BAD_REQUEST, "Invalid service name");
			return;
		}
		
		IServiceHandler servHdl = null;
		
		switch (servName) {
		case simple:
			logger.log(req, ELogType.info, "[ServiceSelector]: selecting simple service");
			servHdl = createSimpleServiceHandler();
			break;
		case correlation:
			logger.log(req, ELogType.info, "[ServiceSelector]: selecting correlation service");
			servHdl = createCorrelationServiceHandler();
			break;
		case convolution:
			logger.log(req, ELogType.info, "[ServiceSelector]: selecting convolution service");
			servHdl = createConvolutionServiceHandler();
			break;
		}
		
		servHdl.handle(req, res);
		
	}
	
	protected IServiceHandler createSimpleServiceHandler() {
		return new SimpleServiceHandler(logger);
	}
	
	protected IServiceHandler createCorrelationServiceHandler() {
		return new CorrelationServiceHandler(logger);
	}
	
	protected IServiceHandler createConvolutionServiceHandler() {
		return new ConvolutionServiceHandler(logger);
	}
}
