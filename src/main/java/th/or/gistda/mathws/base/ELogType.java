package th.or.gistda.mathws.base;

public enum ELogType {

	access("access"), info("info"), debug("debug"), trace("trace");
	
	private String name;
	
	ELogType(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static ELogType parse(String str) {
		if (str.equals(access.getName()))
			return access;
		if (str.equals(info.getName()))
			return info;
		if (str.equals(debug.getName()))
			return debug;
		if (str.equals(trace.getName()))
			return trace;
		
		return null;
	}
}
