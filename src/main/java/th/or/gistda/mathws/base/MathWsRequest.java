package th.or.gistda.mathws.base;

import java.util.List;

import com.sun.net.httpserver.HttpExchange;

public class MathWsRequest extends Object implements IMathWsRequest {

	private HttpExchange ex;
	private String reqId;
	private String servName;
	private String servOp;
	private List<Double> inputA;
	private List<Double> inputB;
	private List<Double> outputC;
	
	@Override
	public void setHttpExchange(HttpExchange ex) {
		this.ex = ex;
	}
	
	@Override
	public HttpExchange getHttpExchange() {
		return ex;
	}
	
	@Override 
	public void setRequestId(String reqId) {
		this.reqId = reqId;
	}
	
	@Override
	public String getRequestId() {
		return reqId;
	}
	
	@Override
	public void setName(String name) {
		servName = name;
	}
	
	@Override
	public String getName() {
		return servName;
	}

	@Override
	public void setOperation(String op) {
		servOp = op;
	}

	@Override
	public String getOperation() {
		return servOp;
	}

	@Override
	public void setInputA(List<Double> a) {
		inputA = a;
	}

	@Override
	public List<Double> getInputA() {
		return inputA;
	}

	@Override
	public void setInputB(List<Double> b) {
		inputB = b;
	}

	@Override
	public List<Double> getInputB() {
		return inputB;
	}

	@Override
	public void setOutputC(List<Double> c) {
		outputC = c;
	}

	@Override
	public List<Double> getOutputC() {
		return outputC;
	}

	@Override
	public boolean equals(Object obj) {
		
		IMathWsRequest compare = (IMathWsRequest) obj;
		
		if ((reqId != null) || (compare.getRequestId() != null)) {
			if (!reqId.equals(compare.getRequestId())) {return false;}
		}
		
		if ((servName != null) || (compare.getName() != null)){
			if (!servName.equals(compare.getName())) {return false;}
		}
		
		if ((servOp != null) || (compare.getOperation() != null)) {
			if (!servOp.equals(compare.getOperation())) {return false;}
		}
		
		if ((inputA != null) || (compare.getInputA() != null)) {
			if (!inputA.equals(compare.getInputA())) {return false;}
		}
		
		if ((inputB != null) || (compare.getInputB() != null)) {
			if (!inputB.equals(compare.getInputB())) {return false;}
		}
		
		if ((outputC != null) || (compare.getOutputC() != null)) {
			if (!outputC.equals(compare.getOutputC())) {return false;}
		}
		
		return true;
	}
}
