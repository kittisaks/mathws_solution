package th.or.gistda.mathws.base;

public interface IMathWsLogger {
	public void log(IMathWsRequest req, ELogType type, String logMsg);
}
