package th.or.gistda.mathws.base;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class CSVLogger implements IMathWsLogger {

	private String filepath = null;
	private FileWriter fw = null;
	private BufferedWriter bw = null;
	
	private String hostname = "Unknown hostname";
	private String hostIP = "Unknow host IP";
	private String remoteIP = "Unknown remote IP";
	
	public CSVLogger(String filepath) throws Exception {
		
		initializeFile(filepath);
		resolveHostnameAndIP();
	}
	
	private void resolveHostnameAndIP() {
		InetAddress ip;
        String hostname;
        try {
            hostname = InetAddress.getLocalHost().getHostName();
            ip = InetAddress.getByName(hostname);
            System.out.println("Your current IP address : " + ip);
            System.out.println("Your current Hostname : " + hostname);
 
        } catch (UnknownHostException e) {
 
            System.out.println("Cannot resolve hostname and/or IP");
        }
	}
	
	private void initializeFile(String filepath) throws Exception {
		this.filepath = filepath;
		
		File logFile = new File(filepath);
		try {
			if (logFile.exists()) {
				fw = new FileWriter(filepath, true);
			}
			else {
				fw = new FileWriter(filepath, false);
			}
		}
		catch (Exception e) {
			System.out.println("Cannot create log file for CSV format.");
			throw e;
		}
		
		bw = new BufferedWriter(fw);
	}
	
	@Override
	public void log(IMathWsRequest req, ELogType type, String logMsg) {
		String remoteIP = req.getHttpExchange().getRemoteAddress().toString();
		String reqId = req.getRequestId();
		try {
			bw.write(hostname + "," + hostIP + "," + remoteIP + "," + reqId + "," + type.getName() + "," + logMsg);
			bw.newLine();
			bw.flush();
		} catch (Exception e) {
			System.out.println("Cannot write to log file for CSV format.");
		}
	}

}
