package th.or.gistda.mathws.base;

public interface IMathWsHandler {
	public void process(String input, String output);
}
