package th.or.gistda.mathws.base;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class RequestValidator_Test {
	
	private void InvalidRequestTest(String requestBody, String expectedErrorMsg) throws Exception {
		//Prepare
		Exception ex = null;
		IMathWsRequest mwsReq = new MathWsRequest();
				
		//Execute
		try {
			RequestValidator.parseRawRequest(requestBody, mwsReq);
		}
		catch (Exception e) {
			ex = e;
		}
				
		//Validate
		assertEquals(ex.getMessage(), expectedErrorMsg);
				
		throw ex;
	}
	
	@Test(expected = Exception.class)
	public void test_empty_ExpectError() throws Exception {
		
		String requestBody = "";
		String expectedErrorMsg = "The request is not a valid Math-WS request";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected = Exception.class)
	public void test_InvalidJson_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "    }"
				+ "    input:{"
				+ "    }"
				+ "}";
		String expectedErrorMsg = "The request is not a valid Math-WS request";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected = Exception.class)
	public void test_NoServiceNoInput_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "}";
		String expectedErrorMsg = "Service section is not specified";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected = Exception.class)
	public void test_Service_NameMissing_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "        operation:\"Service_operation\""
				+ "    },"
				+ "    input:{"
				+ "        a:[1.1, 2.2, 3.3],"
				+ "        b:[4.4, 5.5, 6.6]"
				+ "    }"
				+ "}";
		String expectedErrorMsg = "Service name is not specified";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected=Exception.class)
	public void test_Service_OperationMissing_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "        name:\"Service_name\""
				+ "    },"
				+ "    input:{"
				+ "        a:[1.1, 2.2, 3.3],"
				+ "        b:[4.4, 5.5, 6.6]"
				+ "    }"
				+ "}";
		String expectedErrorMsg = "Service operation is not specified";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected=Exception.class)
	public void test_Input_NoInput_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "        name:\"Service_name\","
				+ "        operation:\"Service_operation\""
				+ "    }"
				+ "}";
		String expectedErrorMsg = "Input section is not specified";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected=Exception.class)
	public void test_Input_AMissing_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "        name:\"Service_name\","
				+ "        operation:\"Service_operation\""
				+ "    },"
				+ "    input:{"
				+ "        b:[4.4, 5.5, 6.6]"
				+ "    }"
				+ "}";
		String expectedErrorMsg = "Input A is not specified";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected=Exception.class)
	public void test_Input_BMissing_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "        name:\"Service_name\","
				+ "        operation:\"Service_operation\""
				+ "    },"
				+ "    input:{"
				+ "        a:[1.1, 2.2, 3.3]"
				+ "    }"
				+ "}";
		String expectedErrorMsg = "Input B is not specified";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected=Exception.class)
	public void test_Input_AandBNotSameLength_ExpectError() throws Exception {
		
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "        name:\"Service_name\","
				+ "        operation:\"Service_operation\""
				+ "    },"
				+ "    input:{"
				+ "        a:[1.1, 2.2],"
				+ "        b:[4.4, 5.5, 6.6]"
				+ "    }"
				+ "}";
		String expectedErrorMsg = "Input A and B do not have the same length";
		
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test(expected=Exception.class)
	public void test_RequestIdMissing_ExpectError() throws Exception {
		
		//Prepare
		String requestBody = 
				  "{"
				+ "    service:{"
				+ "        name:\"Service_name\","
				+ "        operation:\"Service_operation\""
				+ "    },"
				+ "    input:{"
				+ "        a:[1.1, 2.2, 3.3],"
				+ "        b:[4.4, 5.5, 6.6]"
				+ "    }"
				+ "}";
		IMathWsRequest req = new MathWsRequest();
		
		String expectedErrorMsg = "Request identifier is not specified";
		InvalidRequestTest(requestBody, expectedErrorMsg);
	}
	
	@Test
	public void test_Service_Complete_NoError() throws Exception {
		
		//Prepare
		String requestBody = 
				  "{"
				+ "    requestId: \"Test_Request\","
				+ "    service:{"
				+ "        name:\"Service_name\","
				+ "        operation:\"Service_operation\""
				+ "    },"
				+ "    input:{"
				+ "        a:[1.1, 2.2, 3.3],"
				+ "        b:[4.4, 5.5, 6.6]"
				+ "    }"
				+ "}";
		IMathWsRequest req = new MathWsRequest();
		IMathWsRequest expectedReq = new MathWsRequest();
		expectedReq.setRequestId("Test_Request");
		expectedReq.setName("Service_name");
		expectedReq.setOperation("Service_operation");
		expectedReq.setInputA(new ArrayList<Double>() {{add(1.1); add(2.2); add(3.3);}});
		expectedReq.setInputB(new ArrayList<Double>() {{add(4.4); add(5.5); add(6.6);}});
		expectedReq.setOutputC(null);
		
		//Execute
		RequestValidator.parseRawRequest(requestBody, req);
		
		//Verify
		assertEquals(expectedReq, req);
	}
	
}
