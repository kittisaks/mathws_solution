
export mathws_component_name=mathws_solution
export mathws_version=$1
export log_dir="logs"

if [ ! -d $log_dir ]; then
    mkdir $log_dir
fi

if [ -z "$mathws_version" ]; then
   export mathws_version=latest
fi

echo "Starting MathWs with version: $mathws_version"
docker run --rm --name mathws -d -p 8080:8080 -v $(pwd)/$log_dir:/mathws/logs jmse.ddns.net:5000/$mathws_component_name:$mathws_version
if [ ! $? -eq 0 ]; then
    echo "\e[1m\e[31mMathWs failed to start."
else
    echo "\e[1m\e[32mmMathWs started successfully."
fi

